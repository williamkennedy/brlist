<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('home', array('titulo' => 'Página Principal'));
});

Route::get('empresas', 'PesquisaController@VautoComplete');

Route::get('pesquisa', 'PesquisaController@Resultado');

Route::get('empresa/{url}', 'PesquisaController@DetalharEmpresa');

Route::get('avancada', 'PesquisaController@Avancada');

/*Route::get('pesquisa', function(){  
    
    
    return View::make('pesquisa')->with('chave', Input::get('chave'));    
});*/