<?php

class PesquisaController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function Resultado()
	{
            $titulo = 'Pesquisa';            
            /*if(strlen(Input::get('chave')) < 3 ) {                
                $mensagem = '<div class="alert alert-danger">Não é permitido pesquisa termos com menos de 3 letras: "'.Input::get('chave').'"</div>';                
                return View::make('pesquisa', array('chave' => Input::get('chave'), 'mensagem' => $mensagem, 'url' => array('chave'=>Input::get('chave')), 'titulo' => $titulo, 'paramsRamo' => '' ))->with('empresas',  array())->with('estados', array())->with('cidades',  array())->with('ramos', array());
            }*/
            
            $filtro = new Filtro();
            $filtro->chave = Input::get('chave');
            if(Input::get('page'))
                $filtro->pagina = Input::get('page');
            
            $filtro->estado = Input::get('estado');
            $filtro->cidade = Input::get('cidade');
            $filtro->ramo = Input::get('ramo');
            
            Empresa::$filtro = $filtro;
            $empresas = Empresa::Pesquisa($filtro);
            $estados = Empresa::ListaEstado($filtro);
            $cidades = Empresa::ListaCidade($filtro);
            $ramos = Empresa::ListaRamo($filtro);
            $params = array('estado' => '', 'cidade' => '', 'ramo' => '');
            
            if(sizeof($empresas)>0 && !isset($mensagem)){
                $mensagem  =  $empresas->getTotal() . ' empresas encontrados.';
                $appendUrl['chave'] = Input::get('chave');
                
                if(Input::get('estado')){
                    $mensagem .=  ' no estado de ' . Input::get('estado');
                    $appendUrl['estado'] = Input::get('estado');
                    $params['estado'] = '&estado=' . Input::get('estado');
                    
                }
                if(Input::get('cidade')){
                    $mensagem .=  ' na cidade de ' . Input::get('cidade');
                    $appendUrl['cidade'] = input::get('cidade');
                    $params['cidade'] .= '&cidade=' . Input::get('cidade');
                }
                $mensagem .= '. Exibindo de ' . $empresas->getFrom(). ' até ' . $empresas->getTo() . '.';
            }else{
                $mensagem = '<div class="alert alert-danger">Nenhuma empresa foi encontrada com os termos "'.Input::get('chave').'"</div>';                
            }
            
            if(Input::get('ramo'))
            {
                $appendUrl['ramo'] = Input::get('ramo');
                $params['ramo'] = '&ramo=' . Input::get('ramo');
            }
            
            $paramsurl = array();
            foreach($appendUrl as $chave => $valor)
                $paramsurl[] = '&' . $chave . '=' . $valor;
            
            $retorno = array(
                'chave' => $filtro->chave,
                'titulo' => $titulo,
                'mensagem' => $mensagem,
                'url' => $appendUrl,
                'params' => $params
            );
                        
            return View::make('pesquisa', $retorno)->with('empresas', $empresas)->with('estados', $estados)->with('cidades', $cidades)->with('ramos', $ramos);
	}
        
        /* Visualizar a saida do autocomplete */
        protected function VautoComplete(){
            $empresas = Empresa::AutoComplete(Input::get('chave'));
            return View::make('empresas', array('titulo' => 'Empresas'))->with('empresas', $empresas);
        }
        
        /* Detalhamento da empresa */
        protected function DetalharEmpresa($url){
            $empresa = Empresa::where('empUrl', '=', $url)->take(1)->get();
            return View::make('empresa', array('titulo' => $empresa[0]->empNome))->with('empresa', $empresa[0]);
        }
        
        protected function Avancada(){
            $ramos = Empresa::ListaTodosRamos();
            return View::make('avancada', array('titulo' => 'Pesquisa Avançada'))->with('ramos', $ramos);
        }
        
        

}