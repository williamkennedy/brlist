@extends('layout')
    
@section('content')
    <div class="row">
        <div class="col-md-9">
            
            <div  class='panel panel-default'>
                <div class="panel-heading"><h2>{{$empresa->empNome}}<br><small>{{$empresa->empRamo}}</small></h2></div>

                <div class="panel-body">
                    <address>{{$empresa->empEndereco}}<br>
                        <strong>cep: </strong>{{$empresa->empCep}} {{$empresa->Bairro}}<br>
                        {{$empresa->empCidade}} / {{$empresa->empEstado}}
                    </address>
                </div>

                <ul class="list-group">
                    <li class="list-group-item"><h3>Telefones</h3></li>
                    <li class="list-group-item">({{$empresa->empDdd}}) {{$empresa->empFone1}}</li>
                    @if ($empresa->empFone2)
                    <li class="list-group-item">({{$empresa->empDdd}}) {{$empresa->empFone2}}</li>
                    @endif
                    @if ($empresa->empFone3)
                    <li class="list-group-item">({{$empresa->empDdd}}) {{$empresa->empFone3}}</li>
                    @endif
                </ul>
            </div>
            
        </div>
        <div class="col-md-3">
            
            <ul class="nav nav-pills nav-stacked">
                <?php if($empresa->empSite) { ?>
                <li><a href='http://{{$empresa->empSite}}' target="_blank"><span class="glyphicon glyphicon-globe"></span> Website</a></li>
                <?php } ?>
                <li><a href='#'><span class="glyphicon glyphicon-map-marker"></span> Ver no mapa</a></li>
                <?php if($empresa->empEmail) { ?>
                <li><a href='mailto:{{$empresa->empEmail}}'><span class="glyphicon glyphicon-send"></span> E-mail</a></li>
                <?php } ?>
                <li><a href='{{Request::header('referer')}}' style='color:#ED591A'><span class="glyphicon glyphicon-circle-arrow-left"></span> Voltar</a></li>
            </ul>
            
        </div>
    </div>

@stop