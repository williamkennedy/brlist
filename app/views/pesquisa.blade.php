@extends('layout')
    
@section('content')
    <h2>Pesquisa da palavra "<?=$chave?>"</h2>
    
    <div class="container">
        <div class="row target">
            
            <div class="col-md-2"> <!-- Barra Lateral -->
                
                <div class="well"> <!-- Filtros -->
                    <ul class="nav">
                        <li class="nav-header">Filtros</li>
                        
                        <div class="row">&nbsp;</div>
                            <div class="btn-group"> <!-- Estados -->
                                <button class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                    @if(Input::get('estado'))
                                        {{Input::get('estado')}}
                                    @else
                                        Estados
                                    @endif
                                  <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="pesquisa?chave={{$chave}}{{$params['ramo']}}">Todos </a></li>
                                    @foreach($estados as $estado)
                                    <li><a href="pesquisa?chave={{$chave}}&estado={{$estado->empEstado}}{{$params['ramo']}}">{{$estado->empEstado}} <span class='badge'>{{$estado->total}}</span></a></li>
                                    @endforeach
                                </ul>
                            </div>
                        @if(Input::get('estado'))
                        <div class="row">&nbsp;</div>
                            <div class="btn-group"> <!-- Cidades -->
                                <button class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                    @if(Input::get('cidade'))
                                    {{Input::get('cidade')}}
                                    @else
                                    Cidades
                                    @endif
                                  <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="pesquisa?chave={{$chave}}{{$params['estado']}}{{$params['ramo']}}">Todos </a></li>
                                    @foreach($cidades as $cidade)
                                    <li><a href="pesquisa?chave={{$chave}}{{$params['estado']}}&cidade={{$cidade->empCidade}}{{$params['ramo']}}">{{$cidade->empCidade}} <span class='badge'>{{$cidade->total}}</span></a></li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="row">&nbsp;</div>
                            <div class="btn-group"> <!-- Ramos -->
                                <button class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                    @if(Input::get('ramo'))
                                        {{Input::get('ramo')}}
                                    @else
                                        Ramos
                                    @endif
                                  <span class="caret"></span>
                                </button>                                
                                <ul class="dropdown-menu scrollable-menu" role='menu'>
                                    <li><a href="pesquisa?chave={{$chave}}{{$params['estado']}}{{$params['cidade']}}">Todos</a></li>
                                    @foreach($ramos as $ramo)
                                    <li><a href="pesquisa?chave={{$chave}}&ramo={{$ramo->empRamo}}{{$params['estado']}}{{$params['cidade']}}">{{$ramo->empRamo}} <span class='badge'>{{$ramo->total}}</span></a></li>
                                    @endforeach
                                </ul>
                            </div>

                        </li>
                    </ul>
                </div>
            </div>
        
            <div class="col-md-10"><!-- Resultado da pesquisa -->                
                <p><?=$mensagem?></p>
                @foreach($empresas as $empresa)
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4>{{$empresa->empNome}}<br><small style='color:#abcce8'>{{$empresa->empRamo}}</small></h4>
                    </div>
                    <div class="panel-body">
                        {{$empresa->empEndereco}} {{$empresa->empComplemento}}<br>
                        {{$empresa->empCidade}}/{{$empresa->empEstado}}<br>
                        {{$empresa->empCep}}<br>
                        <span class='label label-primary' style='font-size:14px'>{{$empresa->empDdd}}</span> <span class='label label-primary' style='font-size:14px'>{{$empresa->empFone1}}</span> <span class='label label-primary' style='font-size:14px'>{{$empresa->empFone2}}</span> <span class='label label-primary' style='font-size:14px'>{{$empresa->empFone3}}</span>
                        <div style="float:right">                         
                            <button type="button" class="btn btn-default" onclick='javascript:VerEmpresa("{{$empresa->empUrl}}")'>
                                <span class="glyphicon glyphicon-info-sign"></span> Informações
                            </button>
                            
                            <!--<button type="button" class="btn btn-default" onclick='javascript:VerEmpresa("{{$empresa->empUrl}}")'>
                                <span class="glyphicon glyphicon-globe"></span> Website
                            </button>
                            
                            <button type="button" class="btn btn-default">
                                <span class="glyphicon glyphicon-map-marker"></span> Ver no mapa
                            </button>
                            
                            <button type="button" class="btn btn-default">
                                <span class="glyphicon glyphicon-send"></span> E-mail
                            </button>-->
                        </div>
                    </div>
                </div>    
                @endforeach<!-- Paginação -->
                @if ($empresas)
                    {{$empresas->appends($url)->links()}}
                @endif
            </div>            
        </div>
        <script language='javascript'>
            function VerEmpresa(url)
            {
                window.location='empresa/' + url;
            }
        </script>
@stop