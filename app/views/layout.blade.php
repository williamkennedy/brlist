<html>
    <head>
 
    {{ HTML::style('css/bootstrap.css') }}
    {{ HTML::style('css/bootstrap-theme.min.css') }}    
    {{ HTML::style('css/ui-lightness/jquery-ui-min.css') }}
    {{ HTML::style('css/custom.css') }}
    <title> <?=$titulo?> - BRList </title>
        

    </head>
    <body>
        <div class="navbar" role="navigation">
            <div class="container">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/public"><img src="images/brlist_logo.gif" /></a>
              </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        @if($titulo=='Página Principal')
                        <li class="active"><a href="/">Home</a></li>
                        @else
                        <li><a href="/">Home</a></li>
                        @endif
                      <li><a href="avancada">Busca Avançada</a></li>
                      <li><a href="#contact">Serviços</a></li>
                      <li><a href="#contact">Anuncie sua empresa</a></li>
                      <li><a href="#contact">Central do anunciante</a></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>
        
        <div class="container busca">
            <!-- <h1>Aplicação de pesquisa BRAPI</h1>  -->
                <form action="pesquisa" method="get" class="navbar-form" role="form">
                    <div class="col-lg-6">
                      <div class="input-group">
                        <input type="text" id="chave" name="chave" class="form-control" placeholder="digite a palavra chave..." value="<?=Input::get('chave')?>" autocomplete="off">
                        <span class="input-group-btn">
                          <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search btn-sm"></span></button>
                        </span>
                    </div><!-- /input-group -->
                  </div>                        
                    <div class="col-lg-6">
                        <select class="form-control">
                          <option>Buscar por Ramo de Atividade</option>
                          <option>Buscar por Ramo de Atividade</option>
                          <option>Buscar por Ramo de Atividade</option>
                          <option>Buscar por Ramo de Atividade</option>
                          <option>Buscar por Ramo de Atividade</option>
                      </select>
                    </div>
                </form>
        </div>

        <div class="container">
        @yield('content')        
        </div>
        {{ HTML::script('js/jquery.js') }}
        {{ HTML::script('js/jquery-ui.js') }}
        {{ HTML::script('js/bootstrap.min.js') }}
        <script language="javascript">
        //$(function() {
            
                /*$('#chave').autocomplete({
                    
                    source: 'http://localhost:8000/public/empresas?chave=' + $('#chave').val(),
                    minLength: 2,
                    delay: 100,
                    select: function( event, ui ) {
			$("#chave").val( ui.item.empNome);
			return false;
                    }
                }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                    return $( "<li>" )
                    .append( "<a>" + item.empNome + "<br><span style='font-size:10px'>" + item.empCidade + "/" + item.empEstado +"</span></a>" )
                    .appendTo( ul );
                };*/

        //});
        
        $( "#chave" ).autocomplete({
            source: function( request, response ) {

              $.ajax({
                  dataType: "json",
                  type : 'Get',
                  url: 'http://localhost:8000/public/empresas?chave=' + $('#chave').val(), // URL das informações
                  success: function(data) {
                    $('#chave').removeClass('ui-autocomplete-loading');  // hide loading image

                  response(data); // Data = JSON da resposta
                },
                error: function(data) {
                    $('#chave').removeClass('ui-autocomplete-loading');  
                }
              });
            },
            minLength: 3,
            open: function() {

            },
            close: function() {

            },
            focus:function(event,ui) {

            },
            select: function( event, ui ) {
                $("#chave").val( ui.item.empNome);
		return false;
            }
          }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                    return $( "<li>" )
                    .append( "<a>" + item.empNome + "<br><span style='font-size:10px'>" + item.empCidade + "/" + item.empEstado +"</span></a>" )
                    .appendTo( ul );
                };

        </script>
        
    </body>
</html>