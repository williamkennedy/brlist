<?php
class Empresa extends Eloquent{
    protected $table = 'empresa';
    public static $filtro;
    protected static $Where = array();
    
    /* 2DO: Se não houver filtro, não vai funcionar */    
    static function Pesquisa()
    {
        Empresa::SetWhere();
        Paginator::setCurrentPage(Empresa::$filtro->pagina);
        $empresas = Empresa::whereRaw('empNome like "%'.Empresa::$filtro->chave.'%"' . Empresa::$Where['estado'] . Empresa::$Where['cidade'] . Empresa::$Where['ramo'])->paginate(10);
        
        return $empresas;
    }
    
    static function ListaEstado()
    {
        Empresa::SetWhere();
        $estados = Empresa::select(DB::raw('empEstado, count(*) as total'))->whereRaw('empNome like "%'.Empresa::$filtro->chave.'%"' . Empresa::$Where['ramo'])->groupBy('empEstado')->get();
        return $estados;
    }
    
    static function ListaCidade()
    {
        //if(!Empresa::$filtro->estado) return;
        $cidades = Empresa::select(DB::raw('empCidade, count(*) as total'))->whereRaw('empNome like "%'.Empresa::$filtro->chave.'%" and empEstado = "'. Empresa::$filtro->estado .'"' . Empresa::$Where['ramo'])->groupBy('empCidade')->get();        
        return $cidades;
    }
    
    static function ListaRamo()
    {
        $ramos = Empresa::select(DB::raw('empRamo, count(*) as total'))->whereRaw('empNome like "%' . Empresa::$filtro->chave.'%" and empRamo <> ""' . Empresa::$Where['estado'] . Empresa::$Where['cidade'])->groupBy('empRamo')->get();
        return $ramos;
    }
        
    static function AutoComplete($chave)
    {
        $empresas = Empresa::select('empNome', 'empCidade', 'empEstado')->whereRaw('empNome like "'.$chave.'%"')->take(10)->get()->toJson();        
        return $empresas;
    }    
    
    private static function SetWhere()
    {
        Empresa::$Where['estado'] = (Empresa::$filtro->estado) ? ' and empEstado = "' . Empresa::$filtro->estado . '"' : '';
        Empresa::$Where['cidade'] = (Empresa::$filtro->cidade) ? ' and empCidade = "' . Empresa::$filtro->cidade . '"' : '';
        Empresa::$Where['ramo']   = (Empresa::$filtro->ramo) ? ' and empRamo = "' . Empresa::$filtro->ramo . '"' : '';        
    }
    
    static function ListaTodosRamos()
    {
        $ramos = Empresa::select(DB::raw('empRamo, count(*) as total'))->whereRaw('empRamo <> ""')->groupBy('empRamo')->get();
        return $ramos;
    }
    
}