# BR LIST

Novo site da BRList.
Desenvolvimento back-end em PHP por Ricardo Rodrigues @ricardofrr
Desenvolvimento front-end por William Kennedy @williamknn


-----------------------------------
# Commit do Backend

Os arquivos originais foram empacotado em ./brlist1.tar
os arquivos do front, seguindo o padrăo MVC deve ficar em ./public e as views em ./app/views

Os testes do Backend será feito em [http://vm-0.ricardofrr.kd.io/brl/public/](http://vm-0.ricardofrr.kd.io/brl/public/)